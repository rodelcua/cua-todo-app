package com.cua.service;

import com.cua.model.Task;
import com.cua.model.TaskStatus;
import com.cua.test.AbstractTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;

/**
 * Created by rodel on 6/26/16.
 */
public class TaskServiceTest extends AbstractTest {

    @Autowired
    TaskService taskService;

    @Test
    public void testSaveTask() {
        Task task = new Task();

        task.setName("Sample Task");
        task.setTaskStatus(TaskStatus.DONE);

        Task result = taskService.save(task);

        assertEquals(1L, task.getId());
        assertEquals(task.getName(), result.getName());
        assertEquals(task.getTaskStatus(), result.getTaskStatus());
    }
}
