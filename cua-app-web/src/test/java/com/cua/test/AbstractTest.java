package com.cua.test;

/**
 * Created by rodel on 6/26/16.
 */
import org.junit.runner.RunWith;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cua.config.AppConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class,
        initializers = ConfigFileApplicationContextInitializer.class)
@IntegrationTest
@ActiveProfiles("dev")
public abstract class AbstractTest {
}
