package com.cua.repository;

import com.cua.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by rodel on 6/26/16.
 */
@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
}
