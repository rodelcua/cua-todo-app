package com.cua.controller;

import com.cua.controller.dto.ResponseEntityMsgWrapper;
import com.cua.controller.dto.TaskForm;
import com.cua.controller.dto.TaskView;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by rodel on 6/26/16.
 */
public interface TaskController {

    String URL_LIST = "/task/list";
    String URL_SAVE = "/task/save";
    String URL_DELETE = "/task/delete";

    String URL_TASK_STATUS_LIST = "/task/status/list";

    @RequestMapping(value = URL_LIST, method = RequestMethod.GET)
    List<TaskForm> list();

    @RequestMapping(value = URL_SAVE, method = RequestMethod.POST)
    TaskView save(@RequestBody TaskForm taskForm);

    @RequestMapping(value = URL_DELETE, method = RequestMethod.POST)
    ResponseEntityMsgWrapper delete(@RequestBody TaskForm taskForm);

}
