package com.cua.controller.impl;

import com.cua.controller.TaskController;
import com.cua.controller.dto.ResponseEntityMsgWrapper;
import com.cua.controller.dto.TaskForm;
import com.cua.controller.dto.TaskView;
import com.cua.service.TaskService;
import com.cua.util.WebDtoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by rodel on 6/26/16.
 */
@RestController
public class TaskControllerImpl implements TaskController {

    private static Logger LOG = LoggerFactory.getLogger(TaskControllerImpl.class);

    @Autowired
    private TaskService taskService;

    @Override
    public List<TaskForm> list() {
        return WebDtoUtils.wrap(taskService.list(), TaskForm.class);
    }

    @Override
    public TaskView save(@RequestBody TaskForm form) {
        LOG.info("Form: {}", form);
        return WebDtoUtils.wrap(taskService.save(form.bindForm()), TaskView.class);
    }

    @Override
    public ResponseEntityMsgWrapper delete(@RequestBody TaskForm form) {
        LOG.info("Form: {}", form);
        taskService.delete(form.bindForm());
        return new ResponseEntityMsgWrapper("Successfully deleted task");
    }

}
