package com.cua.controller.dto;

/**
 * Created by rodel on 6/26/16.
 */
public class WebForm<T> {

    protected T delegate;

    protected WebForm(final T delegate) {
        this.delegate = delegate;
    }

}

