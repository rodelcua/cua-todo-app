package com.cua.controller.dto;

import com.cua.model.Task;

/**
 * Created by rodel on 6/27/16.
 */
public class TaskView {

    private final Task delegate;

    public TaskView(Task task) {
        this.delegate = task;
    }

    public long getId() {
        return delegate.getId();
    }

    public String getName() {
        return delegate.getName();
    }

    public String getTaskStatus() {
        return delegate.getTaskStatus().getDesc();
    }
}
