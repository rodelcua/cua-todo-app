package com.cua.controller.dto;

import com.cua.model.Task;
import com.cua.model.TaskStatus;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by rodel on 6/26/16.
 */
public class TaskForm extends WebForm<Task> {

    public TaskForm()  {
        super(new Task());
    }

    public TaskForm(Task task) {
        super(task);
    }

    public void setId(long id) {
        delegate.setId(id);
    }

    public long getId() {
        return delegate.getId();
    }

    public String getName() {
        return delegate.getName();
    }

    public void setName(String name) {
        delegate.setName(name);
    }

    public TaskStatus getTaskStatus() {
        return delegate.getTaskStatus();
    }

    public void setTaskStatus(TaskStatus taskStatus) {
        delegate.setTaskStatus(taskStatus);
    }

    public Task bindForm() {
        return delegate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", getId())
                .append("name", getName())
                .append("taskStatus", getTaskStatus())
                .toString();
    }
}
