package com.cua.controller.dto;

/**
 * Created by rodel on 9/28/15.
 *
 * For String Message to be sent via Response Entity
 *
 */
public class ResponseEntityMsgWrapper {

    private String msg;

    public ResponseEntityMsgWrapper(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseEntityMsgWrapper withMsg(String msg) {
        setMsg(msg);
        return this;
    }
}
