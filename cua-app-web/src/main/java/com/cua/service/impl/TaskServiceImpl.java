package com.cua.service.impl;

import com.cua.model.Task;
import com.cua.repository.TaskRepository;
import com.cua.service.TaskService;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rodel on 6/26/16.
 */
@Service
public class TaskServiceImpl implements TaskService {

    private static Logger LOG = LoggerFactory.getLogger(TaskServiceImpl.class);

    @Autowired
    private TaskRepository repo;

    @Override
    public List<Task> list() {
        return Lists.newArrayList(repo.findAll());
    }

    @Override
    public Task save(Task task) {
        LOG.info("Saving task: {}", task);
        return repo.saveAndFlush(task);
    }

    @Override
    public void delete(Task task) {
        LOG.info("Deleting task: {}", task);
        repo.delete(task);
    }
}
