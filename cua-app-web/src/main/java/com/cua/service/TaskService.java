package com.cua.service;

import com.cua.model.Task;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rodel on 6/26/16.
 */
@Service
public interface TaskService {

    List<Task> list();

    Task save(Task task);

    void delete(Task task);
}
