package com.cua.util;

import com.google.common.collect.Lists;

import java.lang.reflect.Constructor;
import java.util.List;

/**
 * Created by rodel on 6/26/16.
 */
public final class WebDtoUtils {

    private WebDtoUtils() {}

    public static <T> List<T> wrap(List<?> list, Class<T> targetClass) {
        List<T> result = Lists.newArrayList();
        for(Object inst : list) {
            result.add(wrap(inst, targetClass));
        }
        return result;
    }

    public static <T> T wrap(Object inst, Class<T> targetClass) {
        try {
            Constructor<T> constructor = targetClass.getConstructor(inst.getClass());
            return constructor.newInstance(inst);
        } catch (Exception e) {
            throw new RuntimeException("Unable to wrap object", e);
        }
    }
}
