package com.cua;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Created by rodel on 6/26/16.
 */
@SpringBootApplication
public class Runner {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(Runner.class, args);
    }
}
