package com.cua.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rodel on 6/26/16.
 */
public enum TaskStatus {

    DONE("DONE"), NOT_DONE("NOT DONE");

    private final String desc;

    TaskStatus(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    private static Map<String, TaskStatus> map = new HashMap<String, TaskStatus>();

    static {
        map.put("DONE", DONE);
        map.put("NOT DONE", NOT_DONE);
        map.put("NOT_DONE", NOT_DONE);
    }

    @JsonCreator
    public static TaskStatus forValue(String value) {
        return map.get(StringUtils.upperCase(value));
    }

}
