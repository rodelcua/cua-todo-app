angular.module('todoapp', ['xeditable', 'ngRoute'])
.controller('TaskController', function($scope, $filter, $http, $window) {

  $scope.tasks = [];

  $scope.statuses = ["DONE", "NOT DONE"];

  $scope.showStatus = function(task) {
    if (task.taskStatus == "DONE")
        return "DONE"
    return "NOT DONE"
  };

  $scope.list = function() {
    $http({
      method: 'GET',
      url: '/task/list'
    }).then(function successCallback(response) {
        $scope.tasks = response.data;
    });
  };

  $scope.saveTask = function(data, id) {
    console.log("data")
    angular.extend(data, {id: id});

    if (data.taskStatus) {
        $http.post('/task/save', data)
            .then(function successCallback(response) {
            return response.data;
        });
    }
    $window.location.reload();
  };

  $scope.removeTask = function(data, index) {

    if (data.taskStatus) {
        var formData = {
            "taskStatus" : data.taskStatus,
            "name" : data.name,
            "id" : data.id
        }

        $http.post('/task/delete', formData)
            .then(function successCallback(response) {
                  return response.data;
        });
    }
    $scope.tasks.splice(index, 1);
  };

  $scope.addTask = function() {
    $scope.inserted = {
      id: $scope.tasks.length+1,
      name: '',
      status: null
    };
    $scope.tasks.push($scope.inserted);
  };

  $scope.validateTask = function(data) {
    if (angular.equals({}, data)) {
        return "Task is invalid. Name and status should not be blank.";
    }
  }

  // html5 validation bug workaround for xeditable.
  $scope.checkData = function(data) {
      console.log("checking data", data);
      if (!data) {
        return "Value cannot be blank";
      } else if (data.length < 4) {
        return "Minimum size is 4"
      }
  }

});